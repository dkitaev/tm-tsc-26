package ru.tsc.kitaev.tm.api.service;

import ru.tsc.kitaev.tm.api.repository.IRepository;
import ru.tsc.kitaev.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
