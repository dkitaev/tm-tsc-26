package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArg();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getCommandByArg(@NotNull String arg);

    void add(@NotNull AbstractCommand command);

}
